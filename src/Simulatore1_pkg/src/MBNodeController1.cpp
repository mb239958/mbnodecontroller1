/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   MBNodeController1.cpp
 * 
 ******************************************************************/
using namespace std;
#include <iostream>

#include <stdint.h> 
#include <stdio.h>
#include <stdlib.h>

#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Twist.h"

//-----------------------------------------------------------------

const double G_minimo_potenziale_attrattivo = 50;

struct S_distanza_e_angolo {
 double dist , angolo;
};

class Controller
{
public:
  Controller(ros::NodeHandle &nh);

  void pioneer_poseCallbackTEST(const geometry_msgs::TransformStamped trans);
  void clockCallbackTEST(const rosgraph_msgs::Clock rclock);
  void timer1CallbackTEST(const ros::TimerEvent &);

  S_distanza_e_angolo distanza_angolo(double x1, double y1, double x2, double y2);

protected:
  ros::Publisher commandPub;
  ros::Time robot_clock;

  // coordinate riferimento 
  double x_riferimento=3.825; 
  double y_riferimento=-0.0250;

  // coordinate ostacoli 
  double max_ostacolo_range = 10;
 
  double x_ostacolo[13] = { 0, -4.225, -1.75, 1.95,   -0.525, 2.95, 1.5,  6.8,   2.10,  4.40,    4.65,  6.475,  -0.725};
  double y_ostacolo[13] = { 0, -0.0750, 2.45, -0.375, -2.075, 2.15, 5.05, 1.45, -2.975, -2.025, -4.65,  -1.675, -4.825};

  double kattrattivo = 12000;
  double krepulsivo = 2000;

  double potenziale_attrattivo_x =0;
  double potenziale_attrattivo_y =0;
  double potenziale_repulsivo_x[13];
  double potenziale_repulsivo_y[13];

  double alfa = 0.00001;
  double vel_lineare = 0;
  double vel_angolare = 0;
  double teta_corrente = 0;
  double PI = 3.141592653;
  double max_dteta = 10; 
  double vel_lineare_corrente = 0;
  double max_vel_lineare = 0.1;
  double max_delta_vel_lineare = 0.1;

};
 
Controller::Controller(ros::NodeHandle &nh)
{
  commandPub = nh.advertise<geometry_msgs::Twist>("cmd_vel", 10);
  ROS_INFO("..creata istanza controller");
};

void Controller::timer1CallbackTEST(const ros::TimerEvent &)
{
  geometry_msgs::Twist msg;

  msg.linear.x = vel_lineare;
  msg.angular.z = vel_angolare;

  // ROS_INFO("--> /cmd_vel : linear e angular %f %f ", msg.linear.x, msg.angular.z);

  commandPub.publish(msg);
}

void Controller::pioneer_poseCallbackTEST(const geometry_msgs::TransformStamped trans)
{
  double x = trans.transform.translation.x;
  double y = trans.transform.translation.y; 
  double z = trans.transform.rotation.z;

  teta_corrente = 2 * asin(z);

  // calcolo distanza
  S_distanza_e_angolo distanza_riferimento=distanza_angolo(x, y, x_riferimento, y_riferimento);
  S_distanza_e_angolo distanza_ostacolo[13];
  double tot_potenziale_repulsivo_x=0;
  double tot_potenziale_repulsivo_y=0;

  double tot_potenziale_x = 0;
  double tot_potenziale_y = 0;

  // calcolo del potenziale attrattivo
  potenziale_attrattivo_x = std::max(distanza_riferimento.dist * kattrattivo * cos(distanza_riferimento.angolo),  G_minimo_potenziale_attrattivo);
  potenziale_attrattivo_y = std::max(distanza_riferimento.dist * kattrattivo * sin(distanza_riferimento.angolo),  G_minimo_potenziale_attrattivo);

  for (short int n=0 ; n<13 ; ++n )  {
    distanza_ostacolo[n] = distanza_angolo(x, y, x_ostacolo[n], y_ostacolo[n]);
    
    // calcolo potenziale repulsivo di ogni ostacolo
    potenziale_repulsivo_x[n]=0;
    potenziale_repulsivo_y[n]=0;

    if (distanza_ostacolo[n].dist <= max_ostacolo_range ){
        potenziale_repulsivo_x[n]= krepulsivo * ((1/max_ostacolo_range) - (1/distanza_ostacolo[n].dist)) * distanza_ostacolo[n].dist * cos(distanza_ostacolo[n].angolo);
    }else {
        potenziale_repulsivo_x[n]=0;
    }

    if (distanza_ostacolo[n].dist <= max_ostacolo_range ){
        potenziale_repulsivo_y[n]= krepulsivo * ((1/max_ostacolo_range) - (1/distanza_ostacolo[n].dist)) * distanza_ostacolo[n].dist * sin(distanza_ostacolo[n].angolo);
    }else {
        potenziale_repulsivo_y[n]=0;
    }   

    tot_potenziale_repulsivo_x= tot_potenziale_repulsivo_x + potenziale_repulsivo_x[n];
    tot_potenziale_repulsivo_y= tot_potenziale_repulsivo_y + potenziale_repulsivo_y[n];
  }


  ROS_INFO("..potenziale attrattivo-> [%f %f] repulsivo-> [%f %f]", 
     potenziale_attrattivo_x, potenziale_attrattivo_y, tot_potenziale_repulsivo_x, tot_potenziale_repulsivo_y);

  //calcolo potenziale totale
  tot_potenziale_x = tot_potenziale_repulsivo_x + potenziale_attrattivo_x;
  tot_potenziale_y = tot_potenziale_repulsivo_y + potenziale_attrattivo_y;

  // calcolo dteta
  double dtetaRep = 0.1*(atan2(vel_lineare_corrente * cos(teta_corrente) + tot_potenziale_repulsivo_x,
                    vel_lineare_corrente * sin(teta_corrente) + tot_potenziale_repulsivo_y) - teta_corrente);
  double dtetaAtt = -(teta_corrente-distanza_riferimento.angolo);
  double dteta= dtetaAtt+dtetaRep;

  // mi assicuro che dteta stia tra + e - PI
  while (dteta > PI ) {
    dteta -= 2* PI;
  } 
  while (dteta < (-PI)) {
    dteta +=  2*PI;
  }  
  dteta = std::min(max_dteta, dteta);
  dteta = std::max(-max_dteta, dteta);

/*   ROS_INFO("vel_lineare_corrente -> : %f",vel_lineare_corrente);
  ROS_INFO("teta_corrente -> : %f",teta_corrente);
  ROS_INFO("alfa -> : %f",alfa);
  ROS_INFO("tot_potenziale_y -> : %f",tot_potenziale_y);
  ROS_INFO("tot_potenziale_x -> : %f",tot_potenziale_x);
 */


  // assegno vel. lineare
  vel_lineare = sqrt((vel_lineare_corrente * sin(teta_corrente) + alfa*tot_potenziale_y) *
                 (vel_lineare_corrente * sin(teta_corrente) + alfa*tot_potenziale_y) +
                 (vel_lineare_corrente * cos(teta_corrente) + alfa*tot_potenziale_x) *
                 (vel_lineare_corrente * cos(teta_corrente) + alfa*tot_potenziale_x));

  vel_lineare = std::min(vel_lineare_corrente + max_delta_vel_lineare,  vel_lineare);

  vel_lineare_corrente = std::max(vel_lineare_corrente - max_delta_vel_lineare, vel_lineare);
  vel_lineare_corrente = std::min(vel_lineare_corrente, max_vel_lineare);
  vel_lineare = std::max(vel_lineare_corrente, 0.0);
  vel_lineare_corrente=vel_lineare;
  
  if(distanza_riferimento.dist<=0.1)
    vel_lineare=0;

  // cerco di fargli evitare gli ostacoli
  for (short int n = 0; n < 13; ++n)
  {
    if (distanza_ostacolo[n].dist <= 1)
    {
      if (distanza_ostacolo[n].angolo <= PI/4)
      {
        dteta = PI / 4;
      }
      else
      {
        dteta = -PI / 4;
      }
    }
  }

  vel_angolare=dteta;

  ROS_INFO("..vel_lineare -> : %f vel_angolare -> : %f",vel_lineare,vel_angolare);

  if (vel_lineare == 0)
      ROS_INFO("..il robot si e' fermato");
}

void Controller::clockCallbackTEST(const rosgraph_msgs::Clock rclock)
{
  robot_clock=rclock.clock;

  // ROS_INFO("..callback /clock setto robot_time: [%f]", robot_clock.toSec());
}

S_distanza_e_angolo Controller::distanza_angolo(double x1, double y1, double x2, double y2)
{
  S_distanza_e_angolo distanza_angolo;
  
  distanza_angolo.dist= sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  distanza_angolo.angolo=atan2(y2 - y1, x2 - x1);

  return(distanza_angolo);
};


int main(int argc, char *argv[])
{
  printf(".. creo rosnode MBNodeController1 \n");

  ros::init(argc, argv, "MBNodeController1");
  ros::NodeHandle node;
  if (!ros::master::check())
    return (0);

  Controller controller(node);

  ros::Subscriber subClock = node.subscribe("/clock", 1, &Controller::clockCallbackTEST, &controller);
  ros::Subscriber subPioneer_pose = node.subscribe("/Pioneer_pose", 1, &Controller::pioneer_poseCallbackTEST, &controller);
  
  ros::Timer timer1 = node.createTimer(ros::Duration(0.1), &Controller::timer1CallbackTEST, &controller);

	// 10 Hz
	ros::Rate loop_rate(10); 

  int count = 0;
  bool ferma = false;
  while (ros::ok() && !ferma)
  {
    // ROS_INFO("----------------------------------------------------------------");
    ros::spinOnce();
    
		loop_rate.sleep();
    ++count;

  /*  per test 
    if (count>=30){ 
      ferma=false;
    }
  */  
  }

  return 0;
}
